#!/usr/bin/python3
# PYTHON_ARGCOMPLETE_OK

import os
import shlex
import pyperclip
import subprocess
import argparse as ap
import argcomplete

def executeCommand(command):
    return subprocess.check_output(['VBoxManage', 'list', command]).decode('utf-8')[:-1].split('\n')

def getVirtualMachineName(line):
    return line[1:-40]

virtualMachineNames = [getVirtualMachineName(line) for line in executeCommand('vms')]

runningVirtualMachines = executeCommand('runningvms')

parser = ap.ArgumentParser()
if len(runningVirtualMachines) == 1:
    parser.add_argument('virtualMachineName', choices = virtualMachineNames, nargs='?')
else:
    parser.add_argument('virtualMachineName', choices = virtualMachineNames)

parser.add_argument('toWrite', nargs='?')
argcomplete.autocomplete(parser)
args = parser.parse_args()

if args.virtualMachineName is None:
    args.virtualMachineName = getVirtualMachineName(runningVirtualMachines[0])

if args.toWrite is None:
    args.toWrite = pyperclip.paste()
    if args.toWrite == '':
        print('The clipboard is empty!')
        exit(1)

DO_SOME_CHARACTERS_REQUIRE_SPACE_AFTERWARDS = False

# Order: normal, shift, alt gr
# Source: https://kbdlayout.info/KBDFR/scancodes+names

KEYBOARD = {
    0x29: ['²'],
    0x02: ['&', '1'],
    0x03: ['é', '2', '~'],
    0x04: ['"', '3', '#'],
    0x05: ["'", '4', '{'],
    0x06: ['(', '5', '['],
    0x07: ['-', '6', '|'],
    0x08: ['è', '7', '`'],
    0x09: ['_', '8', '\\'],
    # Remove ^ as redundant with 0x1a
    0x0a: ['ç', '9'],
    0x0b: ['à', '0', '@'],
    0x0c: [')', '°', ']'],
    0x0d: ['=', '+', '}'],
    0x10: ['a', 'A'],
    0x11: ['z', 'Z'],
    0x12: ['e', 'E'],
    0x13: ['r', 'R'],
    0x14: ['t', 'T'],
    0x15: ['y', 'Y'],
    0x16: ['u', 'U'],
    0x17: ['i', 'I'],
    0x18: ['o', 'O'],
    0x19: ['p', 'P'],
    0x1a: ['^', '¨'],
    0x1b: ['$', '£', '¤'],
    0x1e: ['q', 'Q'],
    0x1f: ['s', 'S'],
    0x20: ['d', 'D'],
    0x21: ['f', 'F'],
    0x22: ['g', 'G'],
    0x23: ['h', 'H'],
    0x24: ['j', 'J'],
    0x25: ['k', 'K'],
    0x26: ['l', 'L'],
    0x27: ['m', 'M'],
    0x28: ['ù', '%'],
    0x2b: ['*', 'µ'],
    0x56: ['<', '>'],
    0x2c: ['w', 'W'],
    0x2d: ['x', 'X'],
    0x2e: ['c', 'C'],
    0x2f: ['v', 'V'],
    0x30: ['b', 'B'],
    0x31: ['n', 'N'],
    0x32: [',', '?'],
    0x33: [';', '.'],
    0x34: [':', '/'],
    0x35: ['!', '§'],
    0x39: [' ']
}

CHARACTERS_REQUIRING_SPACE_AFTERWARDS = '^¨~`' if DO_SOME_CHARACTERS_REQUIRE_SPACE_AFTERWARDS else ''

def release(scancode):
    return int(scancode) + 128

scanCodes = []

for char in args.toWrite:
    for key in KEYBOARD:
        keyChars = KEYBOARD[key]
        if char in keyChars:
            keyCharIndex = keyChars.index(char)
            match keyCharIndex:
                case 0:
                    scanCodes += [key, release(key)]
                case 1:
                    scanCodes += [0x2a, key, release(key), release(0x2a)]
                case _: # 2
                    scanCodes += [0xe0, 0x38, key, release(key) , 0xe0, release(0x38)]
            if char in CHARACTERS_REQUIRING_SPACE_AFTERWARDS:
                scanCodes += [0x39, 0xb9]
            break

# Note that we can't do that to scroll up the Windows lock screen in every case, as if there is the lock screen one enter press is enough, while if there isn't the lock screen, two are required, but then there is a problem in the first case and so on.
scanCodes += [0x1c, release(0x1c)]

scanCodesStr = ' '.join([shlex.quote('{:02x}'.format(scanCode)) for scanCode in scanCodes])

command = f'VBoxManage controlvm {shlex.quote(args.virtualMachineName)} keyboardputscancode {scanCodesStr}'
os.system(command)

